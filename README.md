# Weather Database

This is a demo project which shows how to use the [RPG STOMP client](https://bitbucket.org/m1hael/stomp)
and the [BlueDroplet REST framework](https://bitbucket.org/m1hael/bluedroplet).

For a working demo a messaging system is needed (like RabbitMQ, ActiveMQ, ...).

The project shows how to do the following things:

- make a HTTP request via the SQL interface in SYSTOOLS
- put data on a remote message queue from a messaging system
- retrieve data from a remote message queue
- provide REST API to weather data
- web frontend to the weather data

## Requirements

This following service programs need to be installed (and all dependencies):

- [RPG STOMP client](https://bitbucket.org/m1hael/stomp)
- [BlueDroplet REST framework](https://bitbucket.org/m1hael/bluedroplet).

## Installation

The file `weatherdb.sql` contains the SQL statement which builds the SQL table
for the weather data.

The installation script `setup` is used to build the programs. It needs to know
where to install the objects. The variable `TARGET_LIB` needs to hold the library
name for the objects. Set the library in the following way:

    export TARGET_LIB=MIHAEL

*before* executing the setup script.

As the programs rely on other libraries the place for the copybook
needs to be stated. 

    export INCDIR=/usr/local/include/

## Usage

### TL;DR:

- Insert some cities into the database table `weather_cities`.
- `CALL WEATHERCTL`

### The long Story

The program `WEATHERGET` retrieves the data from REST service and puts it on the
queue. By using the command `WEATHERGET` the RPG program gets the parameters 
passed correctly in contrast to making the call directly to the RPG program on
the command line which may mess up long parameters (more than 32 characters).

    WEATHERGET COUNTRY(DE) CITY(Hamburg)

The program `WEATHERSTO` takes the data from the queue and stores it in the 
database. It doesn't need any parameters.

The program `WEATHERCTL` can be used to start the whole application with just 
one call (without any parameter). It selects all cities from the table 
`WEATHERCTY` (or `weather_cities`) and starts `WEATHERGET` for each every 15
minutes. It also starts the program to fetch the data from queue, `WEATHERSTO`.
It creates a data area `WEATHER` in the current library which controls the 
lifecycle of the program. Just delete the data area to end the program (it is
checked for existence every 15 minutes).

The program `WEATHERRS` provides the REST services. It can be started without
any parameters. The REST service can be tested with

    curl http://ibmi:8484/weather/cities
