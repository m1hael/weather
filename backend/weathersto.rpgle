**FREE

///
// Store weather data
//
// This program retrieves weather data from a remote message queue and stores
// it in the database for further use.
//
// The remote queue is accessed via a STOMP client.
//
// \project Weather Database
// \author Mihael Schmidt
// \date 23.08.2017
///


ctl-opt main(main);


//
// Prototypes
//
/include 'weather_h.rpgle'
/include 'stomp/stomp_h.rpgle'

dcl-pr getObjectDescription extpgm('QUSROBJD');
  receiver char(100);
  length int(10) const;
  format char(10) const;
  qualifiedObjectName char(20) const;
  type char(10) const;
end-pr;


//
// Global stuff
//
dcl-f WEATHERDB disk usage(*output);

dcl-ds weatherDataArea dtaara('WEATHER') len(1) qualified;
  value char(1);
end-ds;


dcl-proc main;
  retrieveWeatherData();
end-proc;


///
// Retrieve weather data from queue
//
// Retrieves the weather data from a remote queue via a STOMP client.
//
///
dcl-proc retrieveWeatherData;

  dcl-s endedAbnormal ind;
  dcl-s client pointer;
  dcl-s frame pointer;
  dcl-s host char(100);
  dcl-s port int(10);
  dcl-s queue varchar(100);
  dcl-ds weatherData likeds(weather_data_t);
  dcl-s user varchar(100);
  dcl-s pass varchar(100);
  dcl-s running ind inz(*on);
  dcl-ds weatherdb_out likerec(WEATHERF00 : *output);

  queue = '/queue/weather';
  host = '93.157.51.125';
  port = 35801;
  user = 'demo';
  pass = 'omed';

  client = stomp_create(%trim(host) : port);
  stomp_setClientId(client : 'weatherstore@pub400.com');
  // use a timeout of 10 seconds to connect to the message queue
  stomp_setTimeout(client : 10000);
  stomp_open(client);

  stomp_command_connect(client : user : pass);

  stomp_command_subscribe(client : queue);

  dow (running);
    frame = stomp_receiveFrame(client);
    // check if a frame was received
    if (frame <> *null);
      weatherData = %str(stomp_frame_getBody(frame));
      stomp_frame_finalize(frame);

      clear weatherdb_out;

      monitor;
        if (weatherData.longitude <> *blank);
          weatherdb_out.longitude = %float(weatherData.longitude);
        endif;
        if (weatherData.latitude <> *blank);
          weatherdb_out.latitude = %float(weatherData.latitude);
        endif;
        weatherdb_out.city = weatherData.city;
        weatherdb_out.country = weatherData.country;
        if (weatherData.temperature <> *blank);
          weatherdb_out.temperatur = %dec(weatherData.temperature : 5 : 2);
        endif;
        weatherdb_out.temp_unit = weatherData.temperature_unit;
        if (weatherData.humidity <> *blank);
          weatherdb_out.humidity = %int(weatherData.humidity);
        endif;
        weatherdb_out.humid_unit = weatherData.humidity_unit;
        if (weatherData.pressure <> *blank);
          weatherdb_out.pressure = %int(weatherData.pressure);
        endif;
        weatherdb_out.press_unit = weatherData.pressure_unit;
        if (weatherData.wind_speed <> *blank);
          weatherdb_out.wind_speed = %dec(weatherData.wind_speed : 5 : 2);
        endif;
        weatherdb_out.cloudiness = weatherData.cloudiness;
        if (weatherData.updated <> *blank);
          // rpg iso format:    2017-07-29-20.45.34.088000
          // webservice format: 2017-07-29T20:20:00
          weatherData.updated = %trimr(%xlate('T:' : '-.' : weatherData.updated)) + '.000000';
          weatherdb_out.updated = %timestamp(weatherData.updated : *ISO);
        endif;
        weatherdb_out.created = %timestamp();
        on-error *all;
          // TODO better error message
          dsply 'Data format not valid. Message skipped.';
      endmon;

      write WEATHERF00 weatherdb_out;
      
    else;
      // check if the program should end
      running = keepRunning();
    endif;

  enddo;

  stomp_command_disconnect(client);

  on-exit endedAbnormal;
    stomp_close(client);
    stomp_finalize(client);
end-proc;

dcl-proc keepRunning;
  dcl-pi *N ind end-pi;
  
  dcl-s receiver char(100);
  dcl-s qualObjectName char(20) inz('WEATHER   *CURLIB');
  
  // check if data area is available in droplet library
  monitor;
    getObjectDescription(receiver : %size(receiver) : 'OBJD0100' : qualObjectName : '*DTAARA');
    // data area available
    
    in weatherDataArea;
    
    return weatherDataArea.value = '1';
    
  on-error *all;
    // not available => stop running
    return *off;
  endmon;
end-proc;