**FREE

///
// Weather Database REST Services
//
// This module provides the REST services needed for the frontend to access
// the weather database.
//
// \project Weather Database
// \author Mihael Schmidt
// \date 23.08.2017
///


ctl-opt main(main);


//-------------------------------------------------------------------------------------------------
// Prototypes
//-------------------------------------------------------------------------------------------------
/include 'bluedroplet/bluedroplet_h.rpgle'
/include 'llist/llist_h.rpgle'
/include 'weather_h.rpgle'

dcl-pr weather_rs_listCities extproc(*dclcase);
  service pointer const;
  connection pointer const;
  message pointer const;
end-pr;


//-------------------------------------------------------------------------------------------------
// Procedures
//-------------------------------------------------------------------------------------------------
dcl-proc main;
  dcl-s service pointer;
  dcl-ds configuration likeds(droplet_config_configuration);
  
  service = droplet_service_create();

  configuration = droplet_config_xml_load('/home/mihael/src/weather/droplet.xml');
  droplet_service_setConfiguration(service : configuration);
  
  droplet_service_addEndPoint(service : %paddr('weather_rs_listCities') : '/weather' : DROPLET_GET);
  dsply 'end point added';

  dsply 'starting ...';
  droplet_service_start(service);
  dsply 'ended';

  droplet_service_finalize(service);
  dsply 'cleaned up';
end-proc;


///
// \brief List Cities
//
// This method returns all cities which are in the weather history database.
///
dcl-proc weather_rs_listCities export;
  dcl-pi *N;
    service pointer const;
    connection pointer const;
    message pointer const;
  end-pi;

  dcl-s abnormallyEnded ind;
  dcl-s cities pointer;
  dcl-s data varchar(200);
  dcl-s headers pointer;
  dcl-s listEntry pointer;

  cities = weather_listCities();
  if (cities = *null);
    droplet_service_serverError(connection : 'Could not retrieve cities.');
    return;
  endif;

  headers = list_create();
  list_addString(headers : 'Transfer-Encoding: chunked');

  droplet_service_sendHead(connection : DROPLET_OK : DROPLET_JSON : headers);

  data = '[ ';
  droplet_service_sendChunk(connection : %addr(data : *DATA) : %len(data));

  listEntry = list_iterate(cities);
  dow (listEntry <> *null);
    data = %str(listEntry);
    listEntry = list_iterate(cities);
    if (listEntry <> *null);
      data += ' , ';
    endif;
    droplet_service_sendChunk(connection : %addr(data : *DATA) : %len(data));
  enddo;

  data = ' ]';
  droplet_service_sendChunk(connection : %addr(data : *DATA) : %len(data));

  // signal the end of the data set by sending an empty chunk.
  data = '';
  droplet_service_sendChunk(connection : %addr(data : *DATA) : %len(data));

  droplet_service_setFlag(connection : MONGOOSE_FLAG_SEND_AND_CLOSE);

  on-exit abnormallyEnded;
    if (cities <> *null);
      list_dispose(cities);
    endif;
end-proc;
