**FREE

///
// Database access to weather data
//
// This program provides access to the weather database.
//
// \author Mihael Schmidt
// \date 23.08.2017
// \project Weather Database
///

ctl-opt nomain;

/include 'weather_h.rpgle'
/include 'llist/llist_h.rpgle'

dcl-proc weather_listCities export;
  dcl-pi *n pointer end-pi;

  dcl-s cities pointer;
  dcl-ds citiesSqlDs dim(100) qualified;
    city char(50);
  end-ds;
  dcl-s numberEntries int(10);
  dcl-s i int(10);
  dcl-s statement char(100);

  statement = 'SELECT DISTINCT(city) FROM weatherdb ORDER BY city';

  EXEC SQL PREPARE stm FROM :statement;
  EXEC SQL DECLARE c CURSOR FOR stm;
  EXEC SQL OPEN c;

  if (sqlcod < 0);
    return *null;
  endif;

  cities = list_create();

  EXEC SQL FETCH FROM c FOR 100 ROWS INTO :citiesSqlDs;
  if (sqlcod >= 0);
    numberEntries = sqlerrd(3);

    for i = 1 to numberEntries;
      list_addString(cities : %trimr(citiesSqlDs(i).city));
    endfor;
  endif;

  EXEC SQL CLOSE c;

  return cities;
end-proc;

dcl-proc weather_getLatestData export;
  dcl-pi *n likeds(weather_data_t);
    city char(50) const;
  end-pi;

  dcl-ds data likeds(weather_data_t) inz;

  // TODO retrieve and return data

  return data;
end-proc;
