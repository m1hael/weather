**FREE

///
// Fetch weather data
//
// This program gets the weather data from the REST service from OpenWeatherMap
// and puts it on a remote queue via the STOMP client.
//
// \project Weather Database
//
// \link http://openweathermap.org OpenWeatherMap
// \link https://bitbucket.org/m1hael/stomp STOMP
//
// \parm Country Code (ISO2/3)
// \parm City Name
///


ctl-opt main(main);


dcl-pr main extpgm('WEATHERGET');
  country char(3);
  city char(255);
end-pr;


/include 'weather_h.rpgle'
/include 'stomp/stomp_h.rpgle'
/include 'message/message_h.rpgle'


// openweathermap API key
dcl-c API_KEY 'abc';
// http://api.openweathermap.org/data/2.5/weather?q={city name},{country code}&mode=xml&APPID=API_KEY
dcl-c API_URL 'http://api.openweathermap.org/data/2.5/weather';

// communications data structure need for the xml bifs
dcl-ds xmldata_t qualified template;
  weather likeds(weather_data_t);
  lastAttrName char(100);
  lastElement char(100);
end-ds;


///
// PEP
///
dcl-proc main;
  dcl-pi *N;
    country char(3);
    city char(255);
  end-pi;

  dcl-s xmlData varchar(32000);
  dcl-ds weatherData likeds(weather_data_t);

  xmlData = fetchWeatherData(country : city);
  weatherData = parseWeatherData(xmlData);
  send(weatherData);
end-proc;

///
// Fetch weather data
//
// Fetches the current weather data from a REST service.
//
// \param Country code ISO (like DE = Germany)
// \param City name
// \return Weather data in XML
///
dcl-proc fetchWeatherData;
  dcl-pi *N varchar(32000);
    country char(3) const;
    city char(255) const;
  end-pi;

  dcl-s result varchar(32000);
  dcl-s url char(300);

  url = API_URL + '?q=' + %trim(city) + ',' + %trim(country) + '&mode=xml&APPID=' + API_KEY;

  EXEC SQL SELECT * INTO :result
           FROM (Values(SYSTOOLS.HTTPGETCLOB(:url, ''))) AS WEATHER;

  if (SQLCOD = 0);
    // everything ok
  elseif (SQLCOD = 100);
    // no data => still ok
  else;
    message_sendEscapeMessage('Could not fetch data from OpenWeatherMap.org for ' 
        +%trimr(city) + '. SQLCODE ' + %char(sqlcod));
  endif;

  return result;
end-proc;


///
// Parse weather data
//
// Transforms the XML data to the weather entry data structure which can be
// used for the queuing the data in the remote message queue.
//
// \param XML weather data
// \return Filled weather entry data structure
///
dcl-proc parseWeatherData;
  dcl-pi *N likeds(weather_data_t);
    data varchar(32000) const;
  end-pi;

  dcl-ds commArea likeds(xmlData_t);

  xml-sax %handler(xmlHandler : commArea) %xml(data);

  return commArea.weather;
end-proc;


///
// XML handler procedure for weather data transformation from xml to ds
///
dcl-proc xmlHandler;
  dcl-pi *N int(10);
    commArea likeds(xmldata_t);
    event int(10) value;
    string pointer value;
    stringLen int(20) value;
    exceptionId int(10) value;
  end-pi;

  dcl-s value char(1000) based(string);
  dcl-s tmp char(100);
  dcl-s retVal int(10);
  dcl-s fieldLength uns(10);
  dcl-s s char(1000);

  select;
    when (event = *XML_START_ELEMENT);
      commArea.lastElement = %subst(value : 1 : stringLen);

    when (event = *XML_ATTR_NAME);
      commArea.lastAttrName = %subst(value : 1 : stringLen);

    when (event = *XML_CHARS);
      if (commArea.lastElement = 'country');
        commArea.weather.country = %subst(value : 1 : stringLen);
      endif;

    when (event = *XML_ATTR_CHARS);
      s = %subst(value : 1 : stringLen);

      if (commArea.lastElement = 'city');
        if (commArea.lastAttrName = 'name');
          commArea.weather.city = s;
        endif;
      elseif (commArea.lastElement = 'coord');
        if (commArea.lastAttrName = 'lon');
          commArea.weather.longitude = s;
        elseif (commArea.lastAttrName = 'lat');
          commArea.weather.latitude = s;
        endif;
      elseif (commArea.lastElement = 'wind');
        if (commArea.lastAttrName = 'speed');
          commArea.weather.wind_speed = s;
        endif;
      elseif (commArea.lastElement = 'temperature');
        if (commArea.lastAttrName = 'value');
          commArea.weather.temperature = s;
        elseif (commArea.lastAttrname = 'unit');
          commArea.weather.temperature_unit = s;
        endif;
      elseif (commArea.lastElement = 'humidity');
        if (commArea.lastAttrName = 'value');
          commArea.weather.humidity = s;
        elseif (commArea.lastAttrName = 'unit');
          commArea.weather.humidity_unit = s;
        endif;
      elseif (commArea.lastElement = 'pressure');
        if (commArea.lastAttrName = 'value');
          commArea.weather.pressure = s;
        elseif (commArea.lastAttrName = 'unit');
          commArea.weather.pressure_unit = s;
        endif;
      elseif (commArea.lastElement = 'clouds');
        if (commArea.lastAttrName = 'name');
          commArea.weather.cloudiness = s;
        endif;
      elseif (commArea.lastElement = 'lastupdate');
        if (commArea.lastAttrName = 'value');
          commArea.weather.updated = s;
        endif;
      endif;

    when (event = *XML_END_ELEMENT);
      commArea.lastAttrName = *blank;
      commArea.lastElement = *blank;

  endsl;

  return retVal;
end-proc;


///
// Send weather data
//
// Sends the weather data to a remote queue via a STOMP client.
//
// \param weather data
///
dcl-proc send;
  dcl-pi *N;
    data likeds(weather_data_t) const;
  end-pi;

  dcl-s abnormallyEnded ind inz(*off);
  dcl-s client pointer;
  dcl-s host char(100);
  dcl-s port int(10);
  dcl-s queue varchar(100);
  dcl-s queueData char(1000);
  dcl-s user varchar(100);
  dcl-s pass varchar(100);

  queue = '/queue/weather';
  host = '93.157.51.125';
  port = 35801;
  user = 'demo';
  pass = 'omed';

  queueData = data + x'00';

  client = stomp_create(%trim(host) : port);
  stomp_setClientId(client : 'weatherget@pub400.com');

  stomp_open(client);

  stomp_command_connect(client : user : pass);
  stomp_command_send(client : queue : queueData);
  stomp_command_disconnect(client);

  on-exit abnormallyEnded;
    stomp_close(client);
    stomp_finalize(client);
end-proc;
