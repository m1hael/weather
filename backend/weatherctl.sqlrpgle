**FREE

///
// Controller for Weather Data Retrieval
//
// This program controls and starts for which city weather data will be 
// retrieved.
//
// \project Weather
// \author Mihael Schmidt
// \date 25.08.2017
///


ctl-opt main(main);


//
// Prototypes
// 
/include 'message/message_h.rpgle'

dcl-pr sleep uns(10) extproc('sleep');
  seconds uns(10) value;
end-pr;

dcl-pr executeCommand extpgm('QCMDEXC');
  command char(32702) options(*varsize) const;
  length packed(15:5) const;
end-pr;

dcl-pr weatherget extpgm('WEATHERGET');
  country char(3);
  city char(255);
end-pr;

dcl-pr getObjectDescription extpgm('QUSROBJD');
  receiver char(100);
  length int(10) const;
  format char(10) const;
  qualifiedObjectName char(20) const;
  type char(10) const;
end-pr;


//
// Global stuff
//
dcl-ds weatherDataArea dtaara('WEATHER') len(1) qualified;
  value char(1);
end-ds;


//
// PEP
//
dcl-proc main;

  dcl-s requestInterval uns(10) inz(900); // 900 seconds = 15 minutes
  dcl-s running ind inz(*on);
  dcl-s abnormallyEnded ind inz(*off);
  dcl-s statement char(100);

  init();

  submitWeatherStoreProgram();
  
  // TODO start rest services
  
  statement = 'SELECT country, city FROM weather_cities ORDER BY country, city';

  EXEC SQL PREPARE stm FROM :statement;
  if (sqlcod < 0);
    message_sendEscapeMessage('Could not read weather_cities database table. ' + 
      'Error on preparing SQL statement. SQLCODE ' + %char(sqlcod));
    return;
  endif;

  dow (running = *on);
    
    processCities();
    
    sleep(900);
  enddo;

  on-exit abnormallyEnded;
    stopWeatherStoreProgram();
end-proc;

dcl-proc init;
  dcl-s command char(100);
  
  command = 'CRTDTAARA DTAARA(WEATHER) TYPE(*CHAR) LEN(1) VALUE(''1'')';
  executeCommand(command : %size(command));
end-proc;

dcl-proc processCities;
  dcl-s numberEntries int(10);
  dcl-s i int(10);
  dcl-s city char(255);
  dcl-ds citiesSqlDs dim(100) qualified;
    country char(3);
    city varchar(255);
  end-ds;
  
  EXEC SQL DECLARE c CURSOR FOR stm;
  EXEC SQL OPEN c;
  
  if (sqlcod < 0);
    message_sendInfoMessage('Could not read weather_cities database table. ' + 
      'Error on opening SQL cursor. SQLCODE ' + %char(sqlcod));
    return;
  endif;
  
  EXEC SQL FETCH FROM c FOR 100 ROWS INTO :citiesSqlDs;
  if (sqlcod >= 0);
    numberEntries = sqlerrd(3);

    for i = 1 to numberEntries;
      monitor;
        city = citiesSqlDs(i).city;
        weatherget(citiesSqlDs(i).country : city);
        on-error *all;
          message_sendInfoMessage('Could not retrieve weather data for ' + 
              %trimr(citiesSqlDs(i).city) + '.');
      endmon;
    endfor;
  endif;
  
  EXEC SQL CLOSE c;
end-proc;

dcl-proc keepRunning;
  dcl-pi *N ind end-pi;
  
  dcl-s receiver char(100);
  dcl-s qualObjectName char(20) inz('WEATHER   *CURLIB');
  
  // check if data area is available in droplet library
  monitor;
    getObjectDescription(receiver : %size(receiver) : 'OBJD0100' : qualObjectName : '*DTAARA');
    // data area available
    
    in weatherDataArea;
    
    return weatherDataArea.value = '1';
    
  on-error *all;
    // not available => stop running
    return *off;
  endmon;
end-proc;

dcl-proc submitWeatherStoreProgram;
  dcl-s command char(100);
  
  command = 'SBMJOB CMD(CALL WEATHERSTO) JOB(WEATHERSTO) JOBQ(QBATCH)';
  executeCommand(command : %size(command));
end-proc;

dcl-proc stopWeatherStoreProgram;
  dcl-s command char(100);
  
  command = 'DLTDTAARA WEATHER';
  executeCommand(command : %size(command));
end-proc;