**FREE

/if not defined (WEATHER_H)
/define WEATHER_H

///
// Weather Entry Data
//
// This data structure contains the weather data. It is used to transfer the 
// weather data from the origin (REST service) over the message system to the 
// database.
// <br/><br/>
// All but the city name and the temperature value are optional.
///
dcl-ds weather_data_t qualified template; 
  longitude char(20);                     
  latitude char(20);                      
  city char(255);                         
  country char(3);                        
  temperature char(10);                   
  temperature_unit char(20);              
  humidity char(10);                      
  humidity_unit char(20);                 
  pressure char(10);                      
  pressure_unit char(20);                 
  wind_speed char(10);                    
  cloudiness char(255);                   
  updated char(30);                       
end-ds;                

///
// List cities
//
// Returns a list of all cities in the weather database. City names are unique.
//
// \return List of city names (name: char(255))
//
// \info The caller needs to take care of disposing the list after usage.
///
dcl-pr weather_listCities pointer extproc(*dclcase) end-pr;

///
// Latest Data for City
//
// Returns the latest data for the passed city. If the database does not contain
// any entry for this city an empty data structure is returned.
//
// \param City name (case sensitive)
// \return Weather data
///
dcl-pr weather_getLatestData likeds(weather_data_t) extproc(*dclcase);
  city char(50) const;
end-pr;

/endif
