CREATE OR REPLACE TABLE weatherdb (                    
  id BIGINT NOT NULL GENERATED ALWAYS AS IDENTITY,     
  longitude FLOAT NOT NULL,                            
  latitude FLOAT NOT NULL,                             
  city VARCHAR(255) NOT NULL,                          
  country CHAR(3) NOT NULL,                            
  temperature FOR temperatur DECIMAL(5, 2) NOT NULL,   
  temperature_unit FOR temp_unit VARCHAR(50) NOT NULL, 
  humidity INT,                                        
  humidity_unit FOR humid_unit VARCHAR(50),            
  pressure INT,                                        
  pressure_unit FOR press_unit VARCHAR(50),            
  wind_speed DECIMAL(6, 2),                            
  cloudiness VARCHAR(255),                             
  updated TIMESTAMP,                                   
  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id)                                     
) RCDFMT weatherf00