CREATE OR REPLACE TABLE weather_cities FOR SYSTEM NAME weathercty (
  country char(3) not null,
  city varchar(255) not null
) RCDFMT weathercf1                                                

CREATE UNIQUE INDEX weather_cities_index FOR SYSTEM NAME weatherc01
ON weather_cities(country, city)                                   
