"use strict";

/* Configuration */

angular.module("weather.configuration", [])
        .constant("version", "1.0.0")
        .constant("config", {
            "url": {
                "city" : "http://opensrc.rzkh.de:8484/weather",
                "help" : "https://bitbucket.org/m1hael/weather.web",
                "postalcodes" : "http://vserver:18080/postalcodes"
            }
        })
        .constant("mapcenter", {
            "lat": 51.0473826,
            "lng": 9.1881993,
            "zoom": 7
        });
