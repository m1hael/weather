'use strict';

angular.module("weather.controllers", []);
angular.module("weather.services", []);

// Declare app level module which depends on filters, and services
angular.module('weather', [
    "toastr",
    "weather.filters",
    "weather.services",
    "weather.directives",
    "weather.controllers",
    "weather.configuration",
    "ui-leaflet",
    "angular-loading-bar"
]);
