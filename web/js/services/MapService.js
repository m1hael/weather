"use strict";

/*
 * Map Service
 *
 * Der Map Service handelt komplett das Verhalten der Karte ab.
 *
 */

angular.module("weather.services").
        factory("mapService", function (leafletData, toastr, config, cityService) {

            // var markerIcon = L.icon({iconUrl: "images/weather-icons_sunny.png" , iconAnchor: [0, 30]});
            var markerIcon = L.icon({iconUrl: "images/marker.png" , iconAnchor: [0, 52]});
            var markers = [];

            // load popup template
            var cityPopupTemplate;

            function addPopupToMarker(marker, weatherData) {
                if (cityPopupTemplate) {
                    marker.bindPopup(composePopupInfo(weatherData));
                }
                else {
                    var jq = jQuery.noConflict();
                    // load template from http resource:
                    jq.get('templates/city_popup.mst').done(function(template) {
                        cityPopupTemplate = template;
                        marker.bindPopup(composePopupInfo(weatherData));
                    });
                }
            }

            function composePopupInfo(weatherData) {
                var weatherSymbol = determineWeatherSymbol(weatherData);
                weatherData.symbol = weatherSymbol;
                Mustache.parse(cityPopupTemplate);
                var rendered = Mustache.render(cityPopupTemplate, weatherData);
                return rendered;
            }

            function determineWeatherSymbol(weatherData) {
                return "wi-day-sunny-overcast";
            }

            var mapFiller = {
                map : null,
                cities : null,
                success : function fillMap(weatherData) {
                    for (var i = 0; i < mapFiller.cities.length; i++) {
                        var city = mapFiller.cities[i];

                        if (city.latitude && city.longitude) {

                            var marker = L.marker([city.latitude, city.longitude]);
                            marker.setIcon(markerIcon);
                            addPopupToMarker(marker, weatherData[city.name]);
                            marker.data = city;
                            markers.push(marker);
                            mapFiller.map.addLayer(marker);

                        }
                        else {
                            toastr.warning("The city " + city.name + " has no coordinates and cannot be displayed on the map.");
                        }
                    }
                }
            }

            return {
                showCities: function (cities) {
                    leafletData.getMap().then(function (map) {
                        mapFiller.map = map;
                        mapFiller.cities = cities;
                        cityService.getWeatherData(null, mapFiller);
                    });
                }
            };

        });
