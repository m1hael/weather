"use strict";

/* Services */

angular.module("weather.services").
        factory("cityService", function($http) {

            return {
                list: function(callback) {
                    //var url = urls.get("city") +"?callback_prefix=JSON_CALLBACK";
                    //var promise = $http.jsonp(url, { cache : true });
                    //promise.
                    //        success(function(data, status) {
                    //            callback.success(data);
                    //        }).
                    //        error(function(data, status, headers, config, statusText) {
                    //            callback.error(status, statusText);
                    //        });
                    var returnValue = [
                        { name : "Hamburg" , country : "DE", latitude : 53.5584902 , longitude : 9.7877404 },
                        { name : "Hannover" , country : "DE", latitude : 52.3795836 , longitude : 9.6213902 },
                        { name : "Bremen" , country : "DE", latitude : 53.1199492 , longitude : 8.5962046 },
                        { name : "Berlin" , country : "DE", latitude : 52.5065133 , longitude : 13.1445587 },
                        { name : "Köln" , country : "DE", latitude : 50.9576191 , longitude : 6.8272409 },
                        { name : "München" , country : "DE", latitude : 48.1548256 , longitude : 11.4017539 },
                        { name : "Frankfurt" , country : "DE", latitude : 50.1211277 , longitude : 8.4964822 },
                        { name : "Heidelberg" , country : "DE", latitude : 49.4057072 , longitude : 8.6135748 },
                        { name : "Heilbronn" , country : "DE", latitude : 49.2300242 , longitude : 9.16236 }
                    ];
                    callback.success(returnValue);
                },
                getWeatherData: function(date, mapFiller) {
                    var data = {
                        Hamburg : {
                            city : mapFiller.cities[0].name,
                            country : "DE",
                            temperature : 14.5,
                            temperature_unit : "°C",
                            humidity : 65,
                            humidity_unit : "%",
                            wind : 4.6,
                            wind_unit : "m/s",
                            pressure : 1600,
                            pressure_unit : null,
                            updated : "2017-09-10 12:10:00"
                        },
                        Hannover : {
                            city :mapFiller.cities[1].name,
                            country : "DE",
                            temperature : 14.5,
                            temperature_unit : "°C",
                            humidity : 65,
                            humidity_unit : "%",
                            wind : 4.6,
                            wind_unit : "m/s",
                            pressure : 1600,
                            pressure_unit : null,
                            updated : "2017-09-10 12:10:00"
                        },
                        Bremen : {
                            city : mapFiller.cities[2].name,
                            country : "DE",
                            temperature : 14.5,
                            temperature_unit : "°C",
                            humidity : 65,
                            humidity_unit : "%",
                            wind : 4.6,
                            wind_unit : "m/s",
                            pressure : 1600,
                            pressure_unit : null,
                            updated : "2017-09-10 12:10:00"
                        },
                        Berlin : {
                            city : mapFiller.cities[3].name,
                            country : "DE",
                            temperature : 14.5,
                            temperature_unit : "°C",
                            humidity : 65,
                            humidity_unit : "%",
                            wind : 4.6,
                            wind_unit : "m/s",
                            pressure : 1600,
                            pressure_unit : null,
                            updated : "2017-09-10 12:10:00"
                        },
                        Köln : {
                            city : mapFiller.cities[4].name,
                            country : "DE",
                            temperature : 14.5,
                            temperature_unit : "°C",
                            humidity : 65,
                            humidity_unit : "%",
                            wind : 4.6,
                            wind_unit : "m/s",
                            pressure : 1600,
                            pressure_unit : null,
                            updated : "2017-09-10 12:10:00"
                        },
                        München : {
                            city : mapFiller.cities[5].name,
                            country : "DE",
                            temperature : 14.5,
                            temperature_unit : "°C",
                            humidity : 65,
                            humidity_unit : "%",
                            wind : 4.6,
                            wind_unit : "m/s",
                            pressure : 1600,
                            pressure_unit : null,
                            updated : "2017-09-10 12:10:00"
                        },
                        Frankfurt : {
                            city : mapFiller.cities[6].name,
                            country : "DE",
                            temperature : 14.5,
                            temperature_unit : "°C",
                            humidity : 65,
                            humidity_unit : "%",
                            wind : 4.6,
                            wind_unit : "m/s",
                            pressure : 1600,
                            pressure_unit : null,
                            updated : "2017-09-10 12:10:00"
                        },
                        Heidelberg : {
                            city : mapFiller.cities[7].name,
                            country : "DE",
                            temperature : 14.5,
                            temperature_unit : "°C",
                            humidity : 65,
                            humidity_unit : "%",
                            wind : 4.6,
                            wind_unit : "m/s",
                            pressure : 1600,
                            pressure_unit : null,
                            updated : "2017-09-10 12:10:00"
                        },
                        Heilbronn : {
                            city : mapFiller.cities[8].name,
                            country : "DE",
                            temperature : 14.5,
                            temperature_unit : "°C",
                            humidity : 65,
                            humidity_unit : "%",
                            wind : null,
                            wind_unit : null,
                            pressure : 1600,
                            pressure_unit : "hpa",
                            updated : "2017-09-10 12:10:00"
                        }
                    };
                    mapFiller.success(data);
                }
            };
        });
