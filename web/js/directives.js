'use strict';

/* Directives */

angular.module('weather.directives', []).
        directive('appVersion', ['version', function(version) {
                return function(scope, element, attributes) {
                    element.text(version);
                };
            }]);
