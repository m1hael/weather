'use strict';

/* Controllers */

angular.module('weather.controllers')
        .controller('NavBarController', function ($rootScope, $scope, $http, toastr, config, cityService) {

            this.getHelpUrl = function () {
                return config.url.help;
            };

        });
