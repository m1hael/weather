'use strict';

/* Controllers */

angular.module('weather.controllers').
        controller("MapController", function($scope, mapcenter, toastr, cityService, mapService) {

                // needed for leaflet to center on map on startup
                angular.extend($scope, {
                    "mapcenter": mapcenter
                    }
                );

                var citiesCallback = {
                    success: function (data, status) {
                        mapService.showCities(data);
                    }
                    ,
                    error: function (status, statusText) {
                        toastr.error("Could not fetch list of cities. " + status + ": " + statusText);
                    }
                };

                cityService.list(citiesCallback);
            });
