module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        clean: ["dist", '.tmp'],
        copy: {
            main: {
                files: [{
                    expand: true,
                    // cwd: '.',
                    src: ['index.html', 'templates/*', '*.html', "images/*", '!js/**', '!**/*.css', 'js/configuration.js', 'components/**'],
                    dest: 'dist/'
                }]
            },
            bootstrap: {
                files: [{
                    expand: true,
                    src: [ "bootstrap.min.css", "*.js", "!Gruntfile.js" ],
                    dest: "dist/",
                    flatten: true
                }]
            },
            fontawesome: {
                files: [{
                    expand: true,
                    src: "fonts/*",
                    dest: "dist/fonts/",
                    flatten: true
                }]
            },
            weathericons: {
                files: [{
                    expand: true,
                    src: "font/*",
                    dest: "dist/font/",
                    flatten: true
                }]
            }
        },
        useminPrepare: {
            html: 'index.html'
        },
        usemin: {
            html: ['dist/index.html']
        },
        uglify: {
            options: {
                report: 'min',
                mangle: false
            }
        },
        bower: {
            install: {
                //just run 'grunt bower:install' and you'll see files from your Bower packages in lib directory
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-usemin');

    // Tell Grunt what to do when we type "grunt" into the terminal
    grunt.registerTask('default', [
        'clean', 'copy', 'useminPrepare', 'concat', 'uglify', 'cssmin', 'usemin'
    ]);
};
